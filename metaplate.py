import contentParser as cP
import rawdataParser as rP
import dirReader as dR
import inspect
import openpyxl as opx
from collections import ChainMap
import pandas as pd
from fuzzywuzzy import fuzz
from pathlib import Path
from functools import partial


class Metaplate:
    """The Metaplate class"""

    def __init__(self, dr: dR.DirReader):
        self.dr = dr
        self.parse_result = []

    def parse(
        self,
        default_parsers=True,
        custom_raw_parsers=None,
        custom_content_parsers=None,
    ):
        """Parse the input directory. Return a list of dictionary."""
        dr = self.dr
        raw_files, content_files = dr.get_file_lists(with_path=False)
        wdir = dr.get_wdir()

        if custom_raw_parsers is None:
            custom_raw_parsers = []
        if custom_content_parsers is None:
            custom_content_parsers = []
        if default_parsers:
            raw_parsers = self._get_parsers(rP) + custom_raw_parsers
            content_parsers = self._get_parsers(cP) + custom_content_parsers
        else:
            raw_parsers = custom_raw_parsers
            content_parsers = custom_content_parsers

        self.parse_result = list(
            dict(
                ChainMap(
                    self._run_parsers(
                        opx.load_workbook(wdir / Path(r)).active, raw_parsers
                    ),
                    self._run_parsers(
                        opx.load_workbook(wdir / Path(c)).active, content_parsers
                    ),
                    {"raw_data_file": r, "content_file": c},
                )
            )
            for r, c in zip(raw_files, content_files)
        )

    def fuzzy_search(
        self,
        keywords: str,
        search_fields=None,
        best_of=10,
        method=fuzz.partial_ratio,
        case_sensitive=False,
    ):
        """Fuzzy search to get desired dataset."""
        parse_result = self.parse_result
        pr_filtered = filter_keyword(parse_result, search_fields)
        pr_filtered_flattened = flatten_dict(pr_filtered)

        if search_fields:  # Make sure the returned dataframe has filename
            pr_filtered_with_filename = filter_keyword(
                parse_result, ["raw_data_file", "content_file"] + search_fields
            )
        else:
            pr_filtered_with_filename = pr_filtered

        if not case_sensitive:
            prff = [d.upper() for d in pr_filtered_flattened]
            kw = keywords.upper()
        else:
            prff = pr_filtered_flattened
            kw = keywords

        # Calculate score
        fuzz_score = [method(kw, d) for d in prff]
        # Sort score
        dict_with_score = zip(pr_filtered_with_filename, fuzz_score)
        dict_with_score_sorted = sorted(
            dict_with_score, key=lambda t: t[-1], reverse=True
        )

        # Trim if best_of is smaller than dataset number
        if len(dict_with_score_sorted) >= best_of:
            dict_with_score_sorted = dict_with_score_sorted[0:best_of]

        # Return a dataframe with score and selected search fields
        return pd.DataFrame(
            map(
                # Add fuzz_score as an extra entry
                lambda t: dict(ChainMap(*[t[0], {"fuzz_score": t[-1]}])),
                dict_with_score_sorted,
            )
        )

    def to_pandas_dataframe(self):
        """Export parse_result to pandas dataFrame."""
        return pd.DataFrame(self.parse_result)

    def select(
        self,
        strains=None,
        conditions=None,
        strains_logic="AND",
        conditions_logic="AND",
        **kwargs
    ):
        """Accurately select acccording to strains, conditions and others."""

        if isinstance(strains, str):
            strains = [strains]
        if isinstance(conditions, str):
            conditions = [conditions]

        # Default selectors
        OR_selector = lambda argument, x: bool(set(argument).intersection(x))
        AND_selector = lambda argument, x: set(argument).issubset(x)

        if strains:
            if strains_logic == "AND":
                select_strains = partial(AND_selector, strains)
            elif strains_logic == "OR":
                select_strains = partial(OR_selector, strains)
            elif callable(strains_logic):
                select_strains = partial(strains_logic, strains)
            else:
                raise RuntimeError("strains_logic must be 'AND', 'OR', or a callable.")
        else:
            select_strains = lambda x: True

        if conditions:
            if conditions_logic == "AND":
                select_conditions = partial(AND_selector, conditions)
            elif strains_logic == "OR":
                select_conditions = partial(OR_selector, conditions)
            elif callable(conditions_logic):
                select_conditions = partial(conditions_logic, conditions)
            else:
                raise RuntimeError(
                    "conditions_logic must be 'AND', 'OR', or a callable."
                )
        else:
            select_conditions = lambda x: True

        maps = {"strains": select_strains, "conditions": select_conditions}

        kwargs_w_strains_conditions = dict(ChainMap(maps, kwargs))

        columns = ["raw_data_file", "content_file"] + list(
            kwargs_w_strains_conditions.keys()
        )

        found_results = [
            d
            for d in self.parse_result
            if apply_selections(d, kwargs_w_strains_conditions)
        ]

        if len(found_results) == 0:
            print("No experiment found!")
            return
        else:
            # Return a dataframe with only columns of interest
            return pd.DataFrame(found_results).loc[:, columns]

    # Utils
    @staticmethod
    def _get_parsers(module):
        """Get the list of pointers to parsers from a module."""
        return list(
            map(
                lambda x: x[-1],  # get function pointer from tuple
                filter(
                    lambda x: x[0][0] != "_",  # remove utils from list
                    inspect.getmembers(module, inspect.isfunction),
                ),
            )
        )

    @staticmethod
    def _run_parsers(ws: opx.worksheet.worksheet.Worksheet, parsers):
        """Run multiple parsers at one time."""
        return dict(ChainMap(*list(map(lambda p: p(ws), parsers))))


def filter_keyword(list_of_dict, search_fields):
    """Filter keyword for each dict in a list."""
    if search_fields:  # filter for desired search_fields
        return list(
            map(
                lambda d: {k: v for k, v in d.items() if k in search_fields},
                list_of_dict,
            )
        )
    else:
        return list_of_dict


def flatten_dict(list_of_dict):
    """Flatten data in each dictionary into a long string for fuzzy search."""
    # Return a string with only values of desired field in each dataset
    return map(lambda d: " ".join([str(v) for v in d.values()]), list_of_dict)


def apply_selections(d: dict, filters: dict):
    """Identify if all keys in d satisfy the filters in key_value."""
    for key, func in filters.items():
        if func(d[key]):
            continue
        else:
            return False
    return True
