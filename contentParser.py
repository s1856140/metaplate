import openpyxl

# Utils
#
# Actual parser


def main_parser(ws: openpyxl.worksheet.worksheet.Worksheet):
    """Parse the content file."""
    content = [
        cell
        for col in ws.iter_cols(
            min_row=2, max_row=9, min_col=2, max_col=13, values_only=True
        )
        for cell in col
        if (cell is not None) and (" in " in cell)
    ]
    strains = sorted(set(cell.split(" in ")[0].strip() for cell in content))
    conditions = sorted(set(cell.split(" in ")[1].strip() for cell in content))
    well_with_cells = len(list(filter(lambda w: w.split(" in ")[0] != "null", content)))
    well_in_use = len(content)
    return {
        "strains": strains,
        "conditions": conditions,
        "well_with_cells": well_with_cells,
        "well_in_use": well_in_use,
        }
