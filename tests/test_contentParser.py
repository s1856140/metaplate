import unittest
import openpyxl as opx
import os
from contentParser import *


class TestContentParser(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # Load example data as standard test set
        dataPath = "./data/"
        rawDataList = [
            f
            for f in os.listdir(dataPath)
            if ("xlsx" in f and "Contents" in f and "Sunrise" not in f)
        ]
        wsList = [opx.load_workbook(dataPath + f).active for f in rawDataList]
        cls.wsList = wsList

        # Load true answers
        # Order: ['ExampleData.xlsx', 'Glu.xlsx', 'HxtA.xlsx', 'GluGal.xlsx', 'HxtB.xlsx']
        strainsTruth = []
        conditionsTruth = []
        well_with_cellsTruth = []
        well_in_useTruth = []

        cls.truth = {
            "strains": strainsTruth,
            "conditions": conditionsTruth,
            "well_with_cells": well_with_cellsTruth,
            "well_in_use": well_in_useTruth,
        }

    @classmethod
    def tearDownClass(cls):
        cls.wsList = None

    def test_main_parser(self):
        for j, ws in enumerate(self.wsList):
            for key in ["strains", "conditions", "well_with_cells", "well_in_use"]:
                # self.assertEqual(main_parser(ws)[key], self.truth[key][j])
                continue
