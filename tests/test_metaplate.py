import unittest
import os
from metaplate import Metaplate
from dirReader import DirReader
import pandas as pd
from fuzzywuzzy import fuzz


class TestMetaplate(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # Load example data as standard test set
        dataPath = "./data/"

        raw_files = [
            "ExampleData.xlsx",
            "Glu.xlsx",
            "HxtA.xlsx",
            "GluGal.xlsx",
            "HxtB.xlsx",
            # "Sunrise.xlsx",
        ]
        content_files = [
            "ExampleDataContents.xlsx",
            "GluContents.xlsx",
            "HxtAContents.xlsx",
            "GluGalContents.xlsx",
            "HxtBContents.xlsx",
            # "SunriseContents.xlsx",
        ]

        dr = DirReader(wdir=dataPath)
        dr.add_from_list(raw_files, content_files)
        cls.dr = dr

    @classmethod
    def tearDownClass(cls):
        cls.wsList = None

    def test_parse(self):
        print("Testing parse()...\n")
        mp = Metaplate(self.dr)
        mp.parse()
        print(mp.to_pandas_dataframe())

    def test_fuzzy_search(self):
        print("Testing fuzzy_search()...\n")
        mp = Metaplate(self.dr)
        mp.parse()
        keyword_cases = ["Mal12 4% Mal", "1% Glu", "0.0625% Gal BY4741"]
        methods = [
            # fuzz.ratio,
            fuzz.partial_ratio,
            fuzz.token_sort_ratio,
            # fuzz.token_set_ratio,
        ]
        for k in keyword_cases:
            for m in methods:
                print(f"keyword is {k}...\n")
                print(f"method is {m}...\n")
                print(
                    mp.fuzzy_search(
                        k, search_fields=["strains", "conditions"], method=m
                    )
                )
                print(mp.fuzzy_search(k, search_fields=None, method=m, best_of=3))
                print("\n")

    def test_select(self):
        print("Testing select()...\n")
        mp = Metaplate(self.dr)
        mp.parse()
        print(mp.select(["Mal12:GFP"], ["2% Raf"]))
        print(mp.select(["Mal12:GFP"], ["2% Raf"], "OR", "OR"))
