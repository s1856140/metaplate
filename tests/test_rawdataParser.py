import unittest
import openpyxl as opx
import os
from rawdataParser import *


class TestRawdataParser(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # Load example data as standard test set
        dataPath = "./data/"
        rawDataList = [
            f
            for f in os.listdir(dataPath)
            if ("xlsx" in f and "Contents" not in f and "Sunrise" not in f)
        ]
        wsList = [opx.load_workbook(dataPath + f).active for f in rawDataList]
        cls.wsList = wsList

        # Load true answers
        # Order: ['ExampleData.xlsx', 'Glu.xlsx', 'HxtA.xlsx', 'GluGal.xlsx', 'HxtB.xlsx']
        machineTruth = [
            "1411011275",
            "1411011275",
            "1006006292",
            "1006006292",
            "1411011275",
        ]
        plateTruth = [
            "Thermo Fisher Scientific-Nunclon 96 Flat Bottom Black Polystyrene Cat. No.: 152036/152037/165305 [NUN96fb_OpticalBottom.pdfx]",
            "Costar 96 Flat Bottom Transparent Polystyrene Cat. No.: 3361/3590/9018/3591/9017/3641/3628/3370/2507/2509/2503/9017/9018/3641/3598/3599/3585/3595/3300/3474 [COS96ft.pdfx]",
            "Thermo Fisher Scientific-Nunclon 96 Flat Bottom Black Polystyrene Cat. No.: 152036/152037/165305 [NUN96fb_OpticalBottom.pdfx]",
            "Costar 96 Flat Bottom Transparent Polystyrene Cat. No.: 3361/3590/9018/3591/9017/3641/3628/3370/2507/2509/2503/9017/9018/3641/3598/3599/3585/3595/3300/3474 [COS96ft.pdfx]",
            "Thermo Fisher Scientific-Nunclon 96 Flat Bottom Black Polystyrene Cat. No.: 152036/152037/165305 [NUN96fb_OpticalBottom.pdfx]",
        ]
        shakingMethodTruth = ["Linear"] * len(wsList)
        shakingDurationTruth = ["550 s"] * len(wsList)
        shakingAmplitudeTruth = ["6 mm"] * len(wsList)
        errorMsgTruth = [None] * len(wsList)
        durationTruth = [
            x / 3600
            for x in [
                86962.2,
                73167.2,
                74589.2,
                77752.3,
                72895.2,
            ]
        ]
        startTimeTruth = [
            "17/03/2017 12:51:57",
            "15/01/2016 16:51:22",
            "22/05/2019 15:05:59",
            "14/07/2016 15:42:16",
            "22/05/2019 15:01:13",
        ]
        channelTruth = [
            {"OD": -1, "GFP": 60, "AutoFL": 60, "mCherry": 100},
            {"OD": -1},
            {
                "OD": -1,
                "GFP_80Gain": 80,
                "AutoFL_80Gain": 80,
                "GFP_65Gain": 65,
                "AutoFL_65Gain": 65,
            },
            {"OD": -1},
            {
                "OD": -1,
                "GFP_80Gain": 80,
                "AutoFL_80Gain": 80,
                "GFP_65Gain": 65,
                "AutoFL_65Gain": 65,
            },
        ]

        cls.truth = {
            "machine": machineTruth,
            "plate_type": plateTruth,
            "shaking_method": shakingMethodTruth,
            "shaking_duration": shakingDurationTruth,
            "shaking_amplitude": shakingAmplitudeTruth,
            "duration": durationTruth,
            "channel": channelTruth,
            "error_msg": errorMsgTruth,
            "start_time": startTimeTruth,
        }

    @classmethod
    def tearDownClass(cls):
        cls.wsList = None

    def test_machine(self):
        for j, ws in enumerate(self.wsList):
            self.assertEqual(machine(ws)["machine"], self.truth["machine"][j])

    def test_plate_type(self):
        for j, ws in enumerate(self.wsList):
            self.assertEqual(plate_type(ws)["plate_type"], self.truth["plate_type"][j])

    def test_shaking(self):
        for j, ws in enumerate(self.wsList):
            for key in ["shaking_method", "shaking_duration", "shaking_amplitude"]:
                self.assertEqual(shaking(ws)[key], self.truth[key][j])

    def test_loop_parser(self):
        for j, ws in enumerate(self.wsList):
            for key in ["channel", "error_msg", "start_time"]:
                self.assertEqual(loop_parser(ws)[key], self.truth[key][j])

    def test_duration(self):
        for j, ws in enumerate(self.wsList):
            self.assertEqual(
                round(duration(ws)["duration"], 2), round(self.truth["duration"][j], 2)
            )

    if __name__ == "__main__":
        unittest.main()
