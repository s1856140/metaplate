import unittest
from dirReader import DirReader
import os


class TestDirReader(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # Load example data as standard test set
        cls.wdir = "./data/"
        cls.raw_files = [
            "ExampleData.xlsx",
            "Glu.xlsx",
            "HxtA.xlsx",
            "GluGal.xlsx",
            "HxtB.xlsx",
            "Sunrise.xlsx",
        ]
        cls.content_files = [
            "ExampleDataContents.xlsx",
            "GluContents.xlsx",
            "HxtAContents.xlsx",
            "GluGalContents.xlsx",
            "HxtBContents.xlsx",
            "SunriseContents.xlsx",
        ]

    def test_add_from_list(self):
        dr = DirReader(wdir=self.wdir)

        print("Adding from list...")
        dr.add_from_list(self.raw_files, self.content_files)

    def test_add_by_regex(self):
        dr = DirReader(wdir=self.wdir)
        # FIXME this raw_regex doesn't recognise "Sunrise.xlsx"
        raw_regex = "^.*[^<Contents>].xlsx$"
        content_func = lambda f: f.split(".xlsx")[0] + "Contents" + ".xlsx"

        print("Adding by regex...")
        dr.add_by_regex(raw_regex, content_func)

    def test_add_by_keywords(self):
        dr = DirReader(wdir=self.wdir)

        print("Adding by keywords...")
        dr.add_by_keywords(content_suffix="Contents", raw_name_not_contain="Sunrise")

    def test_adding_files_multiple_times(self):
        dr = DirReader(wdir=self.wdir)
        dr.add_by_keywords(content_suffix="Contents", raw_name_not_contain=["Hxt","Sunrise"])
        dr.add_by_keywords(content_suffix="Contents", raw_name_contains="Hxt")
        dr.add_by_keywords(content_suffix="Contents")
        dr.add_by_keywords(content_suffix="Contents")

    if __name__ == "__main__":
        unittest.main()
