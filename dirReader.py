import os
import re
import glob
from pathlib import Path


class DirReader:
    """dirReader class."""

    def __init__(self, wdir=None, recursive=True, print_when_added=False):
        """define configs upon initiation."""
        self.raw_files = []
        self.content_files = []
        if wdir:
            self.wdir = Path(wdir).resolve()
        else:
            self.wdir = Path(os.getcwd()).resolve()

        if recursive:
            ls = self.wdir.glob("**/*.xlsx")
        else:
            ls = self.wdir.glob("*.xlsx")
        self.all_files = [str(f.relative_to(self.wdir)) for f in ls]

        self.print_when_added = print_when_added

        print(f"Found {len(self.all_files)} files in {self.wdir}.")

    def add_from_list(self, raw_files, content_files):
        """Load files from list."""
        if len(raw_files) != len(content_files):
            raise RuntimeError(
                "Number of raw data files must equal \
            number of content files."
            )

        self._addfiles(raw_files, content_files)

    def add_by_regex(self, raw_regex, content_func):
        """Load files from regex."""
        raw_files = [f for f in self.all_files if re.search(raw_regex, f)]
        content_files = list(map(content_func, raw_files))
        self._addfiles(raw_files, content_files)

    def add_by_keywords(
        self,
        raw_name_contains="",
        raw_name_not_contain="",
        content_prefix="",
        content_suffix="",
        content_func=None,
    ):
        """Load files by keywords in name."""
        ls = self.all_files

        if raw_name_contains:
            if isinstance(raw_name_contains, str):
                raw_files = [f for f in ls if raw_name_contains in f]
            elif isinstance(raw_name_contains, list):
                raw_files = [
                    f for f in ls if any(ele in f for ele in raw_name_contains)
                ]
            else:
                raw_files = ls
        else:
            raw_files = ls

        # Further filter filelist if raw_name_not_contain
        if raw_name_not_contain:
            if isinstance(raw_name_not_contain, str):
                raw_files = [f for f in raw_files if raw_name_not_contain not in f]
            elif isinstance(raw_name_not_contain, list):
                raw_files = [
                    f
                    for f in raw_files
                    if not any(ele in f for ele in raw_name_not_contain)
                ]

        # Further filter filelist if content_prefix or content_suffix
        if content_prefix or content_suffix:
            raw_files = [
                f
                for f in raw_files
                if not (content_prefix in f and content_suffix in f)
            ]

            # contentFilelist generated from rawdata file with prefix or suffix.
            content_files = [
                content_prefix + f[:-5] + content_suffix + ".xlsx" for f in raw_files
            ]

        # content_func is only inspected if no prefix or suffix is provided
        elif content_func:
            content_files = map(content_func, raw_files)
        else:
            raise RuntimeError("Content name pattern undefined.")

        self._addfiles(raw_files, content_files)

    # Utils
    def file_number(self):
        """Show loaded file number."""
        number = len(self.raw_files)
        print(f"In total {number} files are loaded.")
        return number

    def inspect_loaded_files(self, maxnumber=None):
        """Inspect loaded files."""
        for j, r, c in enumerate(zip(self.raw_files, self.content_files)):
            if maxnumber and (j >= maxnumber):
                return
            else:
                print(f"{j} {r} {c}\n")

    def get_file_lists(self, with_path=False):
        """Return file lists."""
        if with_path:
            add_path = lambda f: str(self.wdir / Path(f))
            return map(add_path, self.raw_files), map(add_path, self.content_files)
        else:
            return self.raw_files, self.content_files

    def get_wdir(self):
        """Get current wdir."""
        return self.wdir

    # Internal methods
    def _addfiles(self, raw_files, content_files):
        """Check existence and add *new* files to list."""
        self._checkfiles(raw_files, content_files)
        counter = 0
        for r, c in zip(raw_files, content_files):
            if r in self.raw_files:
                continue
            else:
                self.raw_files.append(r)
                self.content_files.append(c)
                counter += 1
        print(f"Added {counter} new datasets.")
        if self.print_when_added:
            for j, r, c in enumerate(zip(raw_files, content_files)):
                print(f"{j} {r} {c}\n")

    def _checkfiles(self, raw_files, content_files):
        """Check existence of files to be added."""
        for f in raw_files + content_files:
            if not (self.wdir / Path(f)).exists():
                raise RuntimeError(f"Cannot find file {f}.")
