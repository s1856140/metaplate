import openpyxl
# Utils
def _channel_row(ws:openpyxl.worksheet.worksheet.Worksheet):
    """
    Get number of channels and row to start for next part of parsing.
    """
    col = next(iter(ws.iter_cols(min_row=1, max_col=1, max_row=100)))
    # iterate over A1:A50
    for cell in col:
        if isinstance(cell.value, str):
            if cell.value == "Label: OD":
                return 1, cell.row
            elif "Labels" in cell.value:
                return int(cell.value.split()[0]), cell.row


def _max_time(ws:openpyxl.worksheet.worksheet.Worksheet, row):
    """
    Find the maximum time of a row in a worksheet.
    TODO implement _max_time().
    """
    row = next(iter(ws.iter_rows(min_row=row, max_row=row, max_col=10000)))
    max_time = 0.0
    for cell in row:
        if isinstance(cell.value, float):
            if cell.value > max_time:
                max_time = cell.value
        elif cell.value is None:
            break
        else:
            continue
    return max_time


# Actual Parsers
def machine(ws:openpyxl.worksheet.worksheet.Worksheet):
    return {"machine": ws["E2"].value.split(": ")[1]}


def plate_type(ws):
    return {"plate_type": ws["E11"].value}


def shaking(ws:openpyxl.worksheet.worksheet.Worksheet):
    """
    Parse shaking methods, duration and amplitude.
    TODO Include orbital shaking type.
    """
    if "Linear" in ws["A20"].value:
        method = "Linear"
    else:
        method = "Unknown"

    duration = f'{ws["E20"].value} {ws["F20"].value}'
    amplitude = f'{ws["E21"].value} {ws["F21"].value}'

    return {
        "shaking_method": method,
        "shaking_duration": duration,
        "shaking_amplitude": amplitude,
    }


def loop_parser(ws:openpyxl.worksheet.worksheet.Worksheet):
    """
    Parse the body of the worksheet.
    Returns channel parameters, error message and start time.
    """

    channel_num, search_from = _channel_row(ws)

    gains = []
    labels = []
    error_msg = None
    start = None
    counter = 0

    col = next(iter(ws.iter_cols(min_row=search_from, max_col=1, max_row=1000)))
    # Iterate from A1 to A1000.
    for cell in col:
        if isinstance(cell.value, str):
            v = cell.value
            if v == "Gain":
                g = ws.cell(row=cell.row, column=5)
                gains.append(int(g.value))

            if "Error:" in v:
                e = ws.cell(row=cell.row, column=2)
                error_msg = e.value

            if "Start Time:" in v:
                t = ws.cell(row=cell.row, column=2)
                start = t.value

            if v == "Cycle Nr.":
                L = ws.cell(row=cell.row - 1, column=1)
                labels.append(L.value)
                counter += 1

        if counter == channel_num:
            break

    gains = [-1] + gains  # gain is not defined for OD.

    if len(labels) == 1:
        # Single channel will have a 'None' label.
        labels = ["OD"]

    channel = dict(zip(labels, gains))  # Store as dictionary

    return {"channel": channel, "error_msg": error_msg, "start_time": start}


def duration(ws:openpyxl.worksheet.worksheet.Worksheet):
    """
    Parse the duration of an experiment.
    """

    col = next(iter(ws.iter_cols(min_row=1, max_col=1, max_row=1000)))
    # Iterate from A1 to A1000.
    for cell in col:
        if isinstance(cell.value, str):
            if cell.value == "Time [s]":
                return {"duration": _max_time(ws, row=cell.row) / 3600}
