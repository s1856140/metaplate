from setuptools import setup

setup(
    name='metaplate',
    py_modules=['metaplate'],
    author='Yu Huo',
    author_email='yu.huo@ed.ac.uk',
    url='',
    license='MIT',
    description=
    'A package for parsing and annotating metadata of plate reader datasets.',
    python_requires='>=3.6',
    include_package_data=True,
    install_requires=[
        'numpy', 'pandas', 'openpyxl',
        'omniplate@git+https://git.ecdf.ed.ac.uk/pswain/omniplate@master'
    ])
